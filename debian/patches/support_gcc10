Author: Aaron M. Ucko <ucko@debian.org>
Date:   Sun Apr 26 11:20:45 2020 -0400
Description: Support GCC 10+.
Debian-Bug: 957581.

Formally recognize GCC 10 and up, even if (as in Debian) -dumpversion
reports only one or two components of the version number.  Corresponds to

https://www.ncbi.nlm.nih.gov/viewvc/v1?view=revision&revision=88985
https://www.ncbi.nlm.nih.gov/viewvc/v1?view=revision&revision=88988
https://www.ncbi.nlm.nih.gov/viewvc/v1?view=revision&revision=88989
https://www.ncbi.nlm.nih.gov/viewvc/v1?view=revision&revision=89711

though the last of those doesn't apply cleanly without some
intervening changes for better Clang support.

--- a/c++/src/build-system/config.h.in
+++ b/c++/src/build-system/config.h.in
@@ -966,7 +966,7 @@
 /* Compiler name */
 #undef NCBI_COMPILER_UNKNOWN
 
-/* Compiler version as three-digit integer */
+/* Compiler version as three- or four-digit integer */
 #undef NCBI_COMPILER_VERSION
 
 /* Compiler name */
--- a/c++/src/build-system/configure
+++ b/c++/src/build-system/configure
@@ -8233,9 +8233,10 @@ fi
 case "$compiler:$ncbi_compiler_ver" in
     WorkShop*:?.?? )   ncbi_compiler_sed='s/\([0-9]\)\.\([0-9][0-9]\)/\1\20/' ;;
     WorkShop*:[6-9].?) ncbi_compiler_sed='s/\([0-9]\)\.\([0-9\)/\10\20/' ;;
-    ICC:??.? )  ncbi_compiler_sed='s/\([0-9][0-9]\)\.\([0-9]\)/\1\20/' ;;
-    ICC:??.?.? | \
-    ICC:??.?.?.* ) ncbi_compiler_sed='s/\([0-9][0-9]\)\.\([0-9]\)\.\([0-9]\).*/\1\2\3/' ;;
+    [GI]CC:??.? ) ncbi_compiler_sed='s/\([0-9][0-9]\)\.\([0-9]\)/\1\20/' ;;
+    GCC:??.?.? | ICC:??.?.? | ICC:??.?.?.* )
+       ncbi_compiler_sed='s/\([0-9][0-9]\)\.\([0-9]\)\.\([0-9]\).*/\1\2\3/' ;;
+    GCC:?? )    ncbi_compiler_sed='s/\([0-9][0-9]\)/\100/' ;;
     *:?.?.?)    ncbi_compiler_sed='s/\([0-9]\)\.\([0-9]\)\.\([0-9]\)/\1\2\3/' ;;
     *:?.??*)    ncbi_compiler_sed='s/\([0-9]\)\.\([0-9][0-9]\).*/\1\2/' ;;
     *:?.?)      ncbi_compiler_sed='s/\([0-9]\).\([0-9]\)/\1\20/' ;;
@@ -8455,10 +8456,7 @@ if test "$with_mt" != "no" ; then
             # Will allegedly support -openmp at some point, but as of 3.4,
             # just parses it as specifying an output filename: -o penmp.
          else
-            case "$compiler_version" in
-               [123]?? | 4[01]? ) ;;
-               * ) : ${OPENMP_FLAGS=-fopenmp} ;;
-            esac
+            : ${OPENMP_FLAGS=-fopenmp}
          fi
          ;;
       ICC )
@@ -8510,16 +8508,14 @@ case "$host_os:$compiler" in
    # "incomplete type is not allowed" should be an error, not a warning!
    CFLAGS="-we70 $CFLAGS"
    CXXFLAGS="-we70 $CXXFLAGS"
-   case "$compiler_version" in
-      1[1-9]?? ) MATH_LIBS=-Wl,-lm ;; # avoid static libimf in shared libs
-   esac
+   MATH_LIBS=-Wl,-lm # avoid static libimf in shared libs
    ;;
 
  linux*:GCC )
     if test -r $srcdir/src/build-system/config.site.ncbi && test -d "$NCBI"
     then
        case "$compiler_version" in
-          5* ) CPPFLAGS="-D_GLIBCXX_USE_CXX11_ABI=0 $CPPFLAGS" ;;
+          5?? ) CPPFLAGS="-D_GLIBCXX_USE_CXX11_ABI=0 $CPPFLAGS" ;;
        esac
     fi
     ;;
@@ -9888,7 +9884,7 @@ esac
 ### large file support (switch to AC_SYS_LARGEFILE?)
 case "$host_os:$compiler:$compiler_version" in
     cygwin* ) with_lfs=no ;;
-    *:GCC:3[4-9]* | *:GCC:[4-9]* | *:ICC:[91]* ) : ${with_lfs=yes} ;;
+    *:GCC:* | *:ICC:* ) : ${with_lfs=yes} ;;
 esac
 
 if test "$with_lfs" = "yes" ; then
@@ -9910,17 +9906,9 @@ wsrx="[$wschars]"
 #### Flags to enable (potentially unsafe) extra optimization.
 if test "$skip_fast_flags" = no -a -z "$DEF_FAST_FLAGS" ; then
    case "$compiler:$compiler_version" in
-      GCC:2* | GCC:344 )
-        # GCC 2.9x sometimes experiences internal errors at high optimization;
-        # GCC 3.4.4 (at least on FreeBSD) meanwhile generates crashprone code.
-        DEF_FAST_FLAGS="-O2"
-        ;;
       GCC:* )
         DEF_FAST_FLAGS="-O3 -finline-functions -fstrict-aliasing"
-        case "$host_os:$host_cpu:$compiler_version" in
-           freebsd* | solaris*:*86*:* | *:4[5-9]? | *:[5-9]?? ) ;;
-           * ) DEF_FAST_FLAGS="$DEF_FAST_FLAGS -ffast-math" ;;
-        esac
+        # DEF_FAST_FLAGS="$DEF_FAST_FLAGS -ffast-math" ;;
         if test "$with_profiling" != "yes"; then
            # incompatible with -pg
            DEF_FAST_FLAGS="$DEF_FAST_FLAGS -fomit-frame-pointer"
@@ -9958,15 +9946,6 @@ if test "$skip_fast_flags" = no -a -z "$
            solaris* )  DEF_FAST_FLAGS="$DEF_FAST_FLAGS -fast $ARCH_CFLAGS" ;;
         esac
         ;;
-      ICC:[1-8]?? )
-        DEF_FAST_FLAGS="-O2"
-        ;;
-      ICC:9?? )
-        DEF_FAST_FLAGS="-O3 -axP -ip" # optimized for Core or P4 w/SSE3 (max)
-        ;;
-      ICC:10?? )
-        DEF_FAST_FLAGS="-O3 -axT -ip" # Core 2 w/SSSE3; axS would cover SSE 4.1
-        ;;
       ICC:* )
         # Core 2; ICC 11 goes up to SSE4.2 (i7), ICC 12 up to CORE-AVX-I
         DEF_FAST_FLAGS="-O3 -axSSSE3 -ip"
@@ -10108,12 +10087,7 @@ echo "$as_me: error: Unable to find stat
     ;;
   GCC:* )
     # Need to specify runpath for compiler-provided libraries
-    case $compiler_version in
-        30*     ) major=3 ;;
-        31*     ) major=4 ;;
-        3[23]*) major=5 ;;
-        *       ) major=6 ;;
-    esac
+    major=6 # true for all supported versions at present
     found=false
     for f in libstdc++.so.$major libgcc_s.so.1 libstdc++.a; do
         path=`$CXX $LDFLAGS -print-file-name=$f`
@@ -10294,9 +10268,6 @@ esac
 if test "$compiler" = "GCC" ; then
    CFLAGS="-Wall -Wno-format-y2k $CFLAGS"
    CXXFLAGS="-Wall -Wno-format-y2k $CXXFLAGS"
-   case "$compiler_version" in
-      2* ) CXXFLAGS="-ftemplate-depth-32 $CXXFLAGS" ;;
-   esac
    case "$host_os" in
       solaris* )  CPPFLAGS="-D__EXTENSIONS__ $CPPFLAGS" ;;
       irix*    )  CXXFLAGS="$CXXFLAGS -D_LANGUAGE_C_PLUS_PLUS" ;;
@@ -13214,13 +13185,11 @@ _ACEOF
         ;;
       linux* | *gnu* ) # | *bsd*
         case "$compiler:$compiler_version" in
-           ICC:9* | ICC:1??? )
+           ICC:* )
              ncbi_cv_prog_cc_wl_no_asu=no
              LIBS="$LIBS -lc"
              C_LIBS="$C_LIBS -lc"
              ;;
-           ICC:* )
-             ;;
            * )
              FORBID_UNDEF='-Wl,--no-undefined'
              ;;
@@ -13963,7 +13932,7 @@ echo "$as_me: error: --with-max-debug=$w
       echo " $LDFLAGS " | $EGREP_Q "$dbgrx1|$dbgrx2" || LDFLAGS="$LDFLAGS -g"
       STRIP="@:"
       case "$compiler:$compiler_version" in
-         GCC:4[89]? | GCC:[5-9]? | GCC:???? )
+         GCC:* )
             # GCC 4.8 defaults to DWARF 4, which Totalview for one
             # can't handle; roll back to version 3 by default.
             echo " $USER_CFLAGS " | $EGREP_Q "$dbgrx1|$dbgrx2" || \
@@ -25344,10 +25313,6 @@ ac_compiler_gnu=$ac_cv_cxx_compiler_gnu
 
 # Avoid using unsupported (and noisily ignored) attributes.
 case "$compiler:$compiler_version" in
-   GCC:2* | GCC:3[01]* )
-      ncbi_cv_c_deprecation_syntax=none
-      ncbi_cv_c_forceinline=inline
-      ;;
    VisualAge:* )
       ncbi_cv_c_forceinline=inline
       ;;
@@ -25585,7 +25550,7 @@ _ACEOF
 fi
 
 case "$compiler:$compiler_version $CFLAGS $CXXFLAGS" in
-   GCC:[4-9]??\ *\ -fvisibility*|ICC:*\ -fvisibility* ) ;;
+   GCC:*\ *\ -fvisibility* | ICC:*\ -fvisibility* ) ;;
    * ) ncbi_cv_c_attribute_visibility_default=unnecessary ;;
 esac
 
@@ -37178,7 +37143,7 @@ _ACEOF
          # when using Boost at all.
          BOOST_INCLUDE="$BOOST_INCLUDE -erroff=reftotemp"
          ;;
-      GCC*:4[7-9]* | GCC*:5*)
+      GCC*)
          # Some portions of Boost also have a lot of "unused" typedefs
          # from concept checking.
          BOOST_INCLUDE="$BOOST_INCLUDE -Wno-unused-local-typedefs"
@@ -37221,6 +37186,7 @@ echo $ECHO_N "checking for Boost library
             *:MSVC)      BOOST_TAG="-vc"  ;;
          esac
          case "$BOOST_TAG" in
+            # Still valid for version 10.x and up?
             -*gcc|-il ) boost_comp_vers=`echo $compiler_version | cut -c1-2` ;;
             *         ) boost_comp_vers='' ;;
          esac
--- a/c++/src/build-system/configure.ac
+++ b/c++/src/build-system/configure.ac
@@ -1257,9 +1257,10 @@ changequote(, )dnl
 case "$compiler:$ncbi_compiler_ver" in
     WorkShop*:?.?? )   ncbi_compiler_sed='s/\([0-9]\)\.\([0-9][0-9]\)/\1\20/' ;;
     WorkShop*:[6-9].?) ncbi_compiler_sed='s/\([0-9]\)\.\([0-9\)/\10\20/' ;;
-    ICC:??.? )  ncbi_compiler_sed='s/\([0-9][0-9]\)\.\([0-9]\)/\1\20/' ;;
-    ICC:??.?.? | \
-    ICC:??.?.?.* ) ncbi_compiler_sed='s/\([0-9][0-9]\)\.\([0-9]\)\.\([0-9]\).*/\1\2\3/' ;;
+    [GI]CC:??.? ) ncbi_compiler_sed='s/\([0-9][0-9]\)\.\([0-9]\)/\1\20/' ;;
+    GCC:??.?.? | ICC:??.?.? | ICC:??.?.?.* )
+       ncbi_compiler_sed='s/\([0-9][0-9]\)\.\([0-9]\)\.\([0-9]\).*/\1\2\3/' ;;
+    GCC:?? )    ncbi_compiler_sed='s/\([0-9][0-9]\)/\100/' ;;
     *:?.?.?)    ncbi_compiler_sed='s/\([0-9]\)\.\([0-9]\)\.\([0-9]\)/\1\2\3/' ;;
     *:?.??*)    ncbi_compiler_sed='s/\([0-9]\)\.\([0-9][0-9]\).*/\1\2/' ;;
     *:?.?)      ncbi_compiler_sed='s/\([0-9]\).\([0-9]\)/\1\20/' ;;
@@ -1287,7 +1288,7 @@ fi
 AC_DEFINE_UNQUOTED(NCBI_COMPILER, "$ncbi_compiler", [Compiler name])
 AC_DEFINE_UNQUOTED(NCBI_COMPILER_$ncbi_compiler, 1, [Compiler name])
 AC_DEFINE_UNQUOTED(NCBI_COMPILER_VERSION, $ncbi_compiler_ver,
-                   [Compiler version as three-digit integer])
+                   [Compiler version as three- or four-digit integer])
 
 
 #### Additional check for pre-defined compilers
@@ -1407,10 +1408,7 @@ if test "$with_mt" != "no" ; then
             # Will allegedly support -openmp at some point, but as of 3.4,
             # just parses it as specifying an output filename: -o penmp.
          else
-            case "$compiler_version" in
-               [[123]]?? | 4[[01]]? ) ;;
-               * ) : ${OPENMP_FLAGS=-fopenmp} ;;
-            esac
+            : ${OPENMP_FLAGS=-fopenmp}
          fi
          ;;
       ICC )
@@ -1462,16 +1460,14 @@ case "$host_os:$compiler" in
    # "incomplete type is not allowed" should be an error, not a warning!
    CFLAGS="-we70 $CFLAGS"
    CXXFLAGS="-we70 $CXXFLAGS"
-   case "$compiler_version" in
-      1[[1-9]]?? ) MATH_LIBS=-Wl,-lm ;; # avoid static libimf in shared libs
-   esac
+   MATH_LIBS=-Wl,-lm # avoid static libimf in shared libs
    ;;
 
  linux*:GCC )
     if test -r $srcdir/src/build-system/config.site.ncbi && test -d "$NCBI"
     then
        case "$compiler_version" in
-          5* ) CPPFLAGS="-D_GLIBCXX_USE_CXX11_ABI=0 $CPPFLAGS" ;;
+          5?? ) CPPFLAGS="-D_GLIBCXX_USE_CXX11_ABI=0 $CPPFLAGS" ;;
        esac
     fi
     ;;
@@ -2046,7 +2042,7 @@ esac
 ### large file support (switch to AC_SYS_LARGEFILE?)
 case "$host_os:$compiler:$compiler_version" in
     cygwin* ) with_lfs=no ;;
-    *:GCC:3[[4-9]]* | *:GCC:[[4-9]]* | *:ICC:[[91]]* ) : ${with_lfs=yes} ;;
+    *:GCC:* | *:ICC:* ) : ${with_lfs=yes} ;;
 esac
 
 if test "$with_lfs" = "yes" ; then
@@ -2068,17 +2064,9 @@ wsrx="[[$wschars]]"
 #### Flags to enable (potentially unsafe) extra optimization.
 if test "$skip_fast_flags" = no -a -z "$DEF_FAST_FLAGS" ; then
    case "$compiler:$compiler_version" in
-      GCC:2* | GCC:344 )
-        # GCC 2.9x sometimes experiences internal errors at high optimization;
-        # GCC 3.4.4 (at least on FreeBSD) meanwhile generates crashprone code.
-        DEF_FAST_FLAGS="-O2"
-        ;;
       GCC:* )
         DEF_FAST_FLAGS="-O3 -finline-functions -fstrict-aliasing"
-        case "$host_os:$host_cpu:$compiler_version" in
-           freebsd* | solaris*:*86*:* | *:4[[5-9]]? | *:[[5-9]]?? ) ;;
-           * ) DEF_FAST_FLAGS="$DEF_FAST_FLAGS -ffast-math" ;;
-        esac
+        # DEF_FAST_FLAGS="$DEF_FAST_FLAGS -ffast-math" ;;
         if test "$with_profiling" != "yes"; then
            # incompatible with -pg
            DEF_FAST_FLAGS="$DEF_FAST_FLAGS -fomit-frame-pointer"
@@ -2116,15 +2104,6 @@ if test "$skip_fast_flags" = no -a -z "$
            solaris* )  DEF_FAST_FLAGS="$DEF_FAST_FLAGS -fast $ARCH_CFLAGS" ;;
         esac
         ;;
-      ICC:[[1-8]]?? )
-        DEF_FAST_FLAGS="-O2"
-        ;;
-      ICC:9?? )
-        DEF_FAST_FLAGS="-O3 -axP -ip" # optimized for Core or P4 w/SSE3 (max)
-        ;;
-      ICC:10?? )
-        DEF_FAST_FLAGS="-O3 -axT -ip" # Core 2 w/SSSE3; axS would cover SSE 4.1
-        ;;
       ICC:* )
         # Core 2; ICC 11 goes up to SSE4.2 (i7), ICC 12 up to CORE-AVX-I
         DEF_FAST_FLAGS="-O3 -axSSSE3 -ip"
@@ -2225,12 +2204,7 @@ case "$compiler:$compiler_version:$with_
     ;;
   GCC:* )
     # Need to specify runpath for compiler-provided libraries
-    case $compiler_version in
-        30*     ) major=3 ;;
-        31*     ) major=4 ;;
-        3[[23]]*) major=5 ;;
-        *       ) major=6 ;;
-    esac
+    major=6 # true for all supported versions at present
     found=false
     for f in libstdc++.so.$major libgcc_s.so.1 libstdc++.a; do
         path=`$CXX $LDFLAGS -print-file-name=$f`
@@ -2388,9 +2362,6 @@ AC_SUBST(USUAL_AND_LIB)
 if test "$compiler" = "GCC" ; then
    CFLAGS="-Wall -Wno-format-y2k $CFLAGS"
    CXXFLAGS="-Wall -Wno-format-y2k $CXXFLAGS"
-   case "$compiler_version" in
-      2* ) CXXFLAGS="-ftemplate-depth-32 $CXXFLAGS" ;;
-   esac
    case "$host_os" in
       solaris* )  CPPFLAGS="-D__EXTENSIONS__ $CPPFLAGS" ;;
       irix*    )  CXXFLAGS="$CXXFLAGS -D_LANGUAGE_C_PLUS_PLUS" ;;
@@ -3029,13 +3000,11 @@ case "$compiler" in
         ;;
       linux* | *gnu* ) # | *bsd*
         case "$compiler:$compiler_version" in
-           ICC:9* | ICC:1??? )
+           ICC:* )
              ncbi_cv_prog_cc_wl_no_asu=no
              LIBS="$LIBS -lc"
              C_LIBS="$C_LIBS -lc"
              ;;
-           ICC:* )
-             ;;
            * )
              FORBID_UNDEF='-Wl,--no-undefined'
              ;;
@@ -3319,7 +3288,7 @@ changequote(, )dnl
       echo " $LDFLAGS " | $EGREP_Q "$dbgrx1|$dbgrx2" || LDFLAGS="$LDFLAGS -g"
       STRIP="@:"
       case "$compiler:$compiler_version" in
-         GCC:4[89]? | GCC:[5-9]? | GCC:???? )
+         GCC:* )
             # GCC 4.8 defaults to DWARF 4, which Totalview for one
             # can't handle; roll back to version 3 by default.
             echo " $USER_CFLAGS " | $EGREP_Q "$dbgrx1|$dbgrx2" || \
@@ -3929,10 +3898,6 @@ AC_LANG_POP(C)
 
 # Avoid using unsupported (and noisily ignored) attributes.
 case "$compiler:$compiler_version" in
-   GCC:2* | GCC:3[[01]]* )
-      ncbi_cv_c_deprecation_syntax=none
-      ncbi_cv_c_forceinline=inline
-      ;;
    VisualAge:* )
       ncbi_cv_c_forceinline=inline
       ;;
@@ -3985,7 +3950,7 @@ if test $ncbi_cv_c_attribute_destructor
 fi
 
 case "$compiler:$compiler_version $CFLAGS $CXXFLAGS" in
-   GCC:[[4-9]]??\ *\ -fvisibility*|ICC:*\ -fvisibility* ) ;;
+   GCC:*\ *\ -fvisibility* | ICC:*\ -fvisibility* ) ;;
    * ) ncbi_cv_c_attribute_visibility_default=unnecessary ;;
 esac
 
@@ -5632,7 +5597,7 @@ ncbi_cv_lib_boost_version=\`get_BOOST_LI
          # when using Boost at all.
          BOOST_INCLUDE="$BOOST_INCLUDE -erroff=reftotemp"
          ;;
-      GCC*:4[[7-9]]* | GCC*:5*)
+      GCC*)
          # Some portions of Boost also have a lot of "unused" typedefs
          # from concept checking.
          BOOST_INCLUDE="$BOOST_INCLUDE -Wno-unused-local-typedefs"
@@ -5672,6 +5637,7 @@ ncbi_cv_lib_boost_version=\`get_BOOST_LI
             *:MSVC)      BOOST_TAG="-vc"  ;;
          esac
          case "$BOOST_TAG" in
+            # Still valid for version 10.x and up?
             -*gcc|-il ) boost_comp_vers=`echo $compiler_version | cut -c1-2` ;;
             *         ) boost_comp_vers='' ;;
          esac
