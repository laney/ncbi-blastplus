#############################################################################
# $Id: CMakeLists.txt 590260 2019-07-25 18:51:06Z foleyjp $
#############################################################################

NCBI_add_library(edit)
NCBI_add_subdirectory(unit_test)

# Include projects from this directory
#include(CMakeLists.edit.lib.txt)

# Recurse subdirectories
#add_subdirectory(unit_test )
