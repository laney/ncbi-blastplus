#############################################################################
# $Id: CMakeLists.ncbi_xreader.lib.txt 567665 2018-07-23 12:58:02Z gouriano $
#############################################################################

NCBI_begin_lib(ncbi_xreader SHARED)
  NCBI_sources(
    dispatcher reader writer processors
    reader_snp seqref blob_id request_result
    reader_id1_base reader_id2_base reader_service incr_time info_cache
  )
  NCBI_add_definitions(NCBI_XREADER_EXPORTS)
  NCBI_uses_toolkit_libraries(id1 id2 xcompress xconnect xobjmgr seqsplit seqset)
  NCBI_project_watchers(vasilche)
NCBI_end_lib()

if(OFF)
#
#
#
add_library(ncbi_xreader
    dispatcher reader writer processors
    reader_snp seqref blob_id request_result
    reader_id1_base reader_id2_base reader_service incr_time info_cache
)
add_dependencies(ncbi_xreader
    id1 id2 seqsplit seqset 
)

target_link_libraries(ncbi_xreader
    id1 id2 xcompress
    xconnect xobjmgr
)
endif()
