#############################################################################
# $Id: CMakeLists.gene_info_unit_test.app.txt 570067 2018-09-04 13:15:28Z gouriano $
#############################################################################

NCBI_begin_app(gene_info_unit_test)
  NCBI_sources(gene_info_test)
  NCBI_uses_toolkit_libraries(gene_info)
  NCBI_project_watchers(madden camacho)
  NCBI_set_test_assets(data)
  NCBI_set_test_requires(Linux)
  NCBI_add_test()
NCBI_end_app()

if(OFF)

add_executable(gene_info_unit_test-app
    gene_info_test
    )

set_target_properties(gene_info_unit_test-app PROPERTIES OUTPUT_NAME gene_info_unit_test)



target_link_libraries(gene_info_unit_test-app
    gene_info test_boost
)
endif()
