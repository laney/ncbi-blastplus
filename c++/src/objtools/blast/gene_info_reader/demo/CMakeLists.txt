#############################################################################
# $Id: CMakeLists.txt 566390 2018-06-28 17:11:39Z gouriano $
#############################################################################

NCBI_project_tags(demo)
NCBI_add_app(gene_info_reader)

# Include projects from this directory
#include(CMakeLists.gene_info_reader.app.txt)

