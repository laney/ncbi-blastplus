#############################################################################
# $Id: CMakeLists.blastdb_format_unit_test.app.txt 570067 2018-09-04 13:15:28Z gouriano $
#############################################################################

NCBI_begin_app(blastdb_format_unit_test)
  NCBI_sources(seq_writer_unit_test seq_formatter_unit_test)
  NCBI_uses_toolkit_libraries(blastdb_format)
  NCBI_project_watchers(zaretska jianye madden camacho fongah2)
  NCBI_set_test_assets(data)
  NCBI_add_test()
NCBI_end_app()

if(OFF)
#
#
#
add_executable(blastdb_format_unit_test-app
    seq_writer_unit_test
)

set_target_properties(blastdb_format_unit_test-app PROPERTIES OUTPUT_NAME blastdb_format_unit_test)



target_link_libraries(blastdb_format_unit_test-app
    blastdb_format test_boost
)
endif()
