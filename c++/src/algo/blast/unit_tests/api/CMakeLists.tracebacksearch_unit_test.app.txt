#############################################################################
# $Id: CMakeLists.tracebacksearch_unit_test.app.txt 570114 2018-09-04 17:09:49Z gouriano $
#############################################################################

NCBI_begin_app(tracebacksearch_unit_test)
  NCBI_sources(tracebacksearch_unit_test)
  NCBI_uses_toolkit_libraries(blast_unit_test_util xblast)
  NCBI_project_watchers(boratyng madden camacho fongah2)
  NCBI_set_test_assets(tracebacksearch_unit_test.ini data)
  NCBI_add_test()
NCBI_end_app()

if(OFF)
#
#
#
add_executable(tracebacksearch_unit_test-app
    tracebacksearch_unit_test
)

set_target_properties(tracebacksearch_unit_test-app PROPERTIES OUTPUT_NAME tracebacksearch_unit_test)



target_link_libraries(tracebacksearch_unit_test-app
    blast_unit_test_util xblast
)
endif()
