#############################################################################
# $Id: CMakeLists.prelimsearch_unit_test.app.txt 570114 2018-09-04 17:09:49Z gouriano $
#############################################################################

NCBI_begin_app(prelimsearch_unit_test)
  NCBI_sources(prelimsearch_unit_test)
  NCBI_uses_toolkit_libraries(blast_unit_test_util xblast)
  NCBI_project_watchers(boratyng madden camacho fongah2)
  NCBI_set_test_assets(prelimsearch_unit_test.ini data)
  NCBI_add_test()
NCBI_end_app()

if(OFF)
#
#
#
add_executable(prelimsearch_unit_test-app
    prelimsearch_unit_test
)

set_target_properties(prelimsearch_unit_test-app PROPERTIES OUTPUT_NAME prelimsearch_unit_test)



target_link_libraries(prelimsearch_unit_test-app
    blast_unit_test_util xblast
)
endif()
