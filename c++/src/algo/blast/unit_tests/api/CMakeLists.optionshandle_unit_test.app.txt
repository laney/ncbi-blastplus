#############################################################################
# $Id: CMakeLists.optionshandle_unit_test.app.txt 570114 2018-09-04 17:09:49Z gouriano $
#############################################################################

NCBI_begin_app(optionshandle_unit_test)
  NCBI_sources(optionshandle_unit_test)
  NCBI_uses_toolkit_libraries(xblast)
  NCBI_project_watchers(boratyng madden camacho fongah2)
  NCBI_set_test_assets(optionshandle_unit_test.ini)
  NCBI_add_test()
NCBI_end_app()

if(OFF)
#
#
#
add_executable(optionshandle_unit_test-app
    optionshandle_unit_test
)

set_target_properties(optionshandle_unit_test-app PROPERTIES OUTPUT_NAME optionshandle_unit_test)



target_link_libraries(optionshandle_unit_test-app
    test_boost xblast
)
endif()
