#############################################################################
# $Id: CMakeLists.regexp.lib.txt 573129 2018-10-23 18:10:21Z gouriano $
#############################################################################
#
#
#
if (ON)
NCBI_begin_lib(regexp)
NCBI_sources(
    pcre_byte_order pcre_chartables pcre_compile pcre_config pcre_dfa_exec pcre_exec pcre_fullinfo
    pcre_get pcre_globals pcre_jit_compile pcre_maketables pcre_newline pcre_ord2utf8 pcre_refcount
    pcre_string_utils pcre_study pcre_tables pcre_ucd pcre_valid_utf8 pcre_version pcre_xclass
    pcreposix
)
NCBI_requires(LocalPCRE)
NCBI_add_definitions(HAVE_CONFIG_H PCRE_NO_UTF16 PCRE_NO_UTF32)
NCBI_project_watchers(ivanov)
NCBI_end_lib()

else()
add_library(regexp
    pcre_byte_order pcre_chartables pcre_compile pcre_config pcre_dfa_exec pcre_exec pcre_fullinfo
    pcre_get pcre_globals pcre_jit_compile pcre_maketables pcre_newline pcre_ord2utf8 pcre_refcount
    pcre_string_utils pcre_study pcre_tables pcre_ucd pcre_valid_utf8 pcre_version pcre_xclass
    pcreposix
)

include_directories(SYSTEM ${includedir}/util/regexp)
add_definitions(-DHAVE_CONFIG_H -DPCRE_NO_UTF16 -DPCRE_NO_UTF32)
endif()

