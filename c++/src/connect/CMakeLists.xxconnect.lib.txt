#############################################################################
# $Id: CMakeLists.xxconnect.lib.txt 587954 2019-06-13 15:41:09Z gouriano $
#############################################################################

NCBI_begin_lib(xxconnect)
  NCBI_sources(${SRC_CXX})
  NCBI_headers(*.hpp)
  NCBI_requires(TLS)
  NCBI_uses_toolkit_libraries(connect xncbi)
  NCBI_project_tags(core)
  NCBI_project_watchers(lavr mcelhany)
NCBI_end_lib()

if(OFF)
add_library(xxconnect
    ${SRC_CXX}
)

target_link_libraries(xxconnect
    connect xncbi ${GNUTLS_LIBS} ${NETWORK_LIBS}
)
endif()
