#############################################################################
# $Id: CMakeLists.blastdb_convert.app.txt 573454 2018-10-29 12:26:42Z gouriano $
#############################################################################

NCBI_begin_app(blastdb_convert)
  NCBI_sources(blastdb_convert)
  NCBI_uses_toolkit_libraries(blastinput writedb)
  NCBI_add_definitions(NCBI_MODULE=BLASTDB)
  NCBI_project_watchers(camacho fongah2)
  NCBI_requires(LMDB -Cygwin)
NCBI_end_app()


if(OFF)
#
#
#
add_executable(blastdb_convert-app
    blastdb_convert
)

set_target_properties(blastdb_convert-app PROPERTIES OUTPUT_NAME blastdb_convert)

include_directories(SYSTEM ${LMDB_INCLUDE})
add_definitions(-DNCBI_MODULE=BLASTDB)

target_link_libraries(blastdb_convert-app
    blastinput writedb
)
endif()
