#############################################################################
# $Id: CMakeLists.seqcode.asn.txt 564454 2018-05-29 13:35:44Z gouriano $
#############################################################################

NCBI_begin_lib(seqcode)
  NCBI_dataspecs(seqcode.asn)
  NCBI_uses_toolkit_libraries(xser)
NCBI_end_lib()


if(OFF)

set(MODULE seqcode)
set(MODULE_IMPORT )
set(MODULE_PATH objects/seqcode)

set(MODULE_EXT "asn")
add_library(seqcode ${MODULE}__ ${MODULE}___)

RunDatatool("${MODULE}" "${MODULE_IMPORT}")

target_link_libraries(seqcode
    xser
)
endif()
