#############################################################################
# $Id: CMakeLists.entrez2cli.lib.txt 573202 2018-10-24 15:34:08Z gouriano $
#############################################################################

NCBI_begin_lib(entrez2cli)
  NCBI_generated_sources(entrez2_client.cpp entrez2_client_.cpp)
  NCBI_uses_toolkit_libraries(entrez2 xconnect)
  NCBI_project_watchers(lavr)
NCBI_end_lib()


if(OFF)
#
add_library(entrez2cli
    entrez2_client entrez2_client_
)

target_link_libraries(entrez2cli
    entrez2 xconnect
)
endif()
